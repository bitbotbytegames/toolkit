#include "XboxOneController.h"

/// <summary>
/// Default constructor for the XboxOneController class
/// </summary>
XboxOneController::XboxOneController()
{
	sf_Joystick_index = 0;
}

/// <summary>
/// Default destructor for the XboxOneController class
/// </summary>
XboxOneController::~XboxOneController()
{
	
}

/// <summary>
/// Update
/// </summary>
void XboxOneController::update()
{
	// X, A, Y and B buttons
	sf::Joystick::isButtonPressed(sf_Joystick_index, 2) ? m_currentState.X = true : m_currentState.X = false; // X Button
	sf::Joystick::isButtonPressed(sf_Joystick_index, 0) ? m_currentState.A = true : m_currentState.A = false; // A Button
	sf::Joystick::isButtonPressed(sf_Joystick_index, 3) ? m_currentState.Y = true : m_currentState.Y = false; // Y Button
	sf::Joystick::isButtonPressed(sf_Joystick_index, 1) ? m_currentState.B = true : m_currentState.B = false; // B Button
	sf::Joystick::isButtonPressed(sf_Joystick_index, 6) ? m_currentState.Back = true : m_currentState.Back = false; // Back button
	sf::Joystick::isButtonPressed(sf_Joystick_index, 7) ? m_currentState.Start = true : m_currentState.Start = false; // Start button

	// Dpad
	m_dpadPovY = sf::Joystick::getAxisPosition(sf_Joystick_index, sf::Joystick::Axis::PovY);
	m_dpadPovY > dpadThreshold ? m_currentState.DpadUp = true : m_currentState.DpadUp = false; // Dpad Up
	m_dpadPovY < -dpadThreshold ? m_currentState.DpadDown = true : m_currentState.DpadDown = false; // Dpad Down

	m_dpadPovX = sf::Joystick::getAxisPosition(sf_Joystick_index, sf::Joystick::Axis::PovX);
	m_dpadPovX < -dpadThreshold ? m_currentState.DpadLeft = true : m_currentState.DpadLeft = false; // Dpad Left
	m_dpadPovX > dpadThreshold ? m_currentState.DpadRight = true : m_currentState.DpadRight = false; // Dpad Right

	// Shoulder buttons
	sf::Joystick::isButtonPressed(sf_Joystick_index, 4) ? m_currentState.LB = true : m_currentState.LB = false; // LB
	sf::Joystick::isButtonPressed(sf_Joystick_index, 5) ? m_currentState.RB = true : m_currentState.RB = false; // RB

	// LT and RT
	m_currentState.LTrigger = sf::Joystick::getAxisPosition(sf_Joystick_index, sf::Joystick::Z);
	m_currentState.RTrigger = sf::Joystick::getAxisPosition(sf_Joystick_index, sf::Joystick::Z);

	// Right Stick
	m_currentState.RightThumbStick.x = sf::Joystick::getAxisPosition(sf_Joystick_index, sf::Joystick::U);
	m_currentState.RightThumbStick.y = sf::Joystick::getAxisPosition(sf_Joystick_index, sf::Joystick::R);
	sf::Joystick::isButtonPressed(sf_Joystick_index, 9) ? m_currentState.RightThumbStickClick = true :
		m_currentState.RightThumbStickClick = false; // Click

	// Left Stick
	m_currentState.LeftThumbStick.x = sf::Joystick::getAxisPosition(sf_Joystick_index, sf::Joystick::X);
	m_currentState.LeftThumbStick.y = sf::Joystick::getAxisPosition(sf_Joystick_index, sf::Joystick::Y);
	sf::Joystick::isButtonPressed(sf_Joystick_index, 8) ? m_currentState.LeftThumbStickClick = true :
		m_currentState.LeftThumbStickClick = false; // Click
}

/// <summary>
/// Check if controller is connected
/// </summary>
bool XboxOneController::isConnected()
{
	return sf::Joystick::isConnected(sf_Joystick_index);
}

/// <summary>
/// Set the index number of the controller
/// </summary>
void XboxOneController::setIndex(int t_index)
{
	sf_Joystick_index = t_index;
}
