#ifndef XBOXONECONTROLLER_H
#define XBOXONECONTROLLER_H

#include <SFML/Graphics.hpp>

#include <iostream>

struct GamePadState
{	
	bool X;
	bool A;
	bool Y;
	bool B;
	bool LB;
	bool RB;
	bool LeftThumbStickClick;
	bool RightThumbStickClick;
	bool DpadUp;
	bool DpadDown;
	bool DpadLeft;
	bool DpadRight;
	bool Start;
	bool Back;
	bool Xbox;
	float RTrigger;
	float LTrigger;
	sf::Vector2f RightThumbStick;
	sf::Vector2f LeftThumbStick;
};

class XboxOneController
{
private:
	const int dpadThreshold = 50;

public:
	XboxOneController();
	~XboxOneController();
	void update();
	bool isConnected();
	void setIndex(int t_index);
	
	static int s_noOfControllers;
	int sf_Joystick_index;
	float m_dpadPovX;
	float m_dpadPovY;
	GamePadState m_currentState;
	GamePadState m_previousState;
};

#endif // !XBOXONECONTROLLER_H