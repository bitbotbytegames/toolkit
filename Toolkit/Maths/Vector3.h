#ifndef VECTOR3_H
#define VECTOR3_H

#include <SFML/Graphics.hpp>
#include <string>

namespace tk
{
	class Vector3
	{
	public:
		Vector3();
		Vector3(double t_x, double t_y, double t_z);
		Vector3(sf::Vector3i t_vector);
		Vector3(sf::Vector3f t_vector);
		Vector3(sf::Vector2i t_vector);
		Vector3(sf::Vector2f t_vector);
		Vector3(sf::Vector2u t_vector);
		~Vector3();

		double length() const;
		double lengthSquared() const;
		Vector3 unit() const;
		void normalise();
		double angleBetween(const Vector3 t_other)const;
		double dot(const Vector3 t_other) const;
		Vector3 projection(const Vector3 t_other)const;
		Vector3 rejection(const Vector3 t_other)const;
		Vector3 crossProduct(const Vector3 t_other)const;
		std::string toString();

		Vector3 operator+(const Vector3 t_right) const;
		Vector3 operator-(const Vector3 t_right) const;
		Vector3 operator*(const double t_scalar) const;
		Vector3 operator/(const double t_divisor) const;
		Vector3 operator+=(const Vector3 t_right);
		Vector3 operator-=(const Vector3 t_right);
		Vector3 operator-();
		bool operator== (const Vector3 t_right) const;
		bool operator!= (const Vector3 t_right) const;

		operator sf::Vector2i() { return sf::Vector2i(static_cast<int>(x), static_cast<int>(y)); };
		operator sf::Vector2f() { return sf::Vector2f(static_cast<float>(x), static_cast<float>(y)); }
		operator sf::Vector2u() { return sf::Vector2u(static_cast<unsigned int>(x), static_cast<unsigned int>(y)); }
		operator sf::Vector3i() { return sf::Vector3i(static_cast<int>(x), static_cast<int>(y), static_cast<int>(z)); };
		operator sf::Vector3f() { return sf::Vector3f(static_cast<float>(x), static_cast<float>(y), static_cast<float>(z)); };
		
		double x;
		double y;
		double z;
	};
}

#endif // !VECTOR3_H