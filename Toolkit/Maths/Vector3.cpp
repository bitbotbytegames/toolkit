#include "Vector3.h"

#define PI 3.14159265358979323846

#pragma region constructors

/// <summary>
/// Default constructor for the Vector3 class
/// </summary>
tk::Vector3::Vector3() : x{ 0.0 }, y{ 0.0 }, z{ 0.0 }
{

}

/// <summary>
/// Constructor taking doubles
/// </summary>
tk::Vector3::Vector3(double t_x, double t_y, double t_z) : x{ static_cast<double>(t_x) }, y{ static_cast<double>(t_y) }, z{ static_cast<double>(t_z) }
{

}

/// <summary>
/// Constructor taking an SFML Vector3i
/// </summary>
tk::Vector3::Vector3(sf::Vector3i t_vector) : x{ static_cast<double>(t_vector.x) }, y{ static_cast<double>(t_vector.y) }, z{ static_cast<double>(t_vector.z) }
{

}

/// <summary>
/// Constructor taking an SFML Vector3f
/// </summary>
tk::Vector3::Vector3(sf::Vector3f t_vector) : x{ static_cast<double>(t_vector.x) }, y{ static_cast<double>(t_vector.y) }, z{ static_cast<double>(t_vector.z) }
{

}

/// <summary>
/// Constructor taking an SFML Vector2i
/// </summary>
tk::Vector3::Vector3(sf::Vector2i t_vector) : x{ static_cast<double>(t_vector.x) }, y{ static_cast<double>(t_vector.y) }, z{ 0.0 }
{

}

/// <summary>
/// Constructor taking an SFML Vector2f
/// </summary>
tk::Vector3::Vector3(sf::Vector2f t_vector) : x{ static_cast<double>(t_vector.x) }, y{ static_cast<double>(t_vector.y) }, z{ 0.0 }
{

}

/// <summary>
/// Constructor taking an SFML Vector2u
/// </summary>
tk::Vector3::Vector3(sf::Vector2u t_vector) : x{ static_cast<double>(t_vector.x) }, y{ static_cast<double>(t_vector.y) }, z{ 0.0 }
{

}

#pragma endregion

/// <summary>
/// Destructor for the Vector3 class
/// </summary>
tk::Vector3::~Vector3()
{

}

#pragma region methods

/// <summary>
/// Length
/// </summary>
double tk::Vector3::length() const
{
	const double result = std::sqrt(x * x + y * y + z * z);
	return result;
}

/// <summary>
/// Length squared
/// </summary>
double tk::Vector3::lengthSquared() const
{
	const double result = x * x + y * y + z * z;
	return result;
}

/// <summary>
/// Unit - Returns a vector with a length of 1
/// </summary>
tk::Vector3 tk::Vector3::unit() const
{
	double a = x / (std::sqrt(x * x + y * y + z * z));
	double b = y / (std::sqrt(x * x + y * y + z * z));
	double c = z / (std::sqrt(x * x + y * y + z * z));
	return Vector3(a, b, c);
}

/// <summary>
/// Normalise - Changes the length of a supplied vector to 1
/// </summary>
void tk::Vector3::normalise()
{
	x = x / (std::sqrt(x * x + y * y + z * z));
	y = y / (std::sqrt(x * x + y * y + z * z));
	z = z / (std::sqrt(x * x + y * y + z * z));
}

/// <summary>
/// Return the angle between two vectors (in degrees)
/// </summary>
double tk::Vector3::angleBetween(const Vector3 t_other) const
{
	double dotAB = (x * t_other.x) + (y * t_other.y) + (z * t_other.z);
	double magA = std::sqrt((x * x) + (y * y) + (z * z));
	double magB = std::sqrt((t_other.x * t_other.x) + (t_other.y * t_other.y) + (t_other.z * t_other.z));
	double magTotal = magA * magB;
	double angle = dotAB / magTotal;
	const double result = acos(angle) * 180.0 / PI;
	return result;
}

/// <summary>
/// Dot product
/// </summary>
double tk::Vector3::dot(const Vector3 t_other) const
{
	const double dotProduct = (x * t_other.x) + (y * t_other.y) + (z * t_other.z);
	return dotProduct;
}

/// <summary>
/// Projection - The answer will be parallel to the supplied vector
/// </summary>
tk::Vector3 tk::Vector3::projection(const Vector3 t_other) const
{
	double dotAB = (x * t_other.x) + (y * t_other.y) + (z * t_other.z);
	double magA = std::sqrt((x * x) + (y * y) + (z * z));
	double division = dotAB / (magA * magA);
	double a = division * x;
	double b = division * y;
	double c = division * z;
	return Vector3(a, b, c);
}

/// <summary>
/// Rejection - The answer will be orthogonal to the supplied vector
/// </summary>
tk::Vector3 tk::Vector3::rejection(const Vector3 t_other) const
{
	double dotAB = (x * t_other.x) + (y * t_other.y) + (z * t_other.z);
	double magA = std::sqrt((x * x) + (y * y) + (z * z));
	double division = dotAB / (magA * magA);
	double a = t_other.x - (division * x);
	double b = t_other.y - (division * y);
	double c = t_other.z - (division * z);
	return Vector3(a, b, c);
}

/// <summary>
/// Cross product
/// </summary>
tk::Vector3 tk::Vector3::crossProduct(const Vector3 t_other) const
{
	return Vector3((y * t_other.z) - (z * t_other.y), (z * t_other.x) - (x * t_other.z), (x * t_other.y) - (y * t_other.x));
}

/// <summary>
/// Create a string for the vector in the form [x,y,z]
/// using std::to_string
/// </summary>
std::string tk::Vector3::toString()
{
	const std::string output = "[" + std::to_string(x) + "," + std::to_string(y) + "," + std::to_string(z) + "]";
	return output;
}

#pragma endregion

#pragma region operators

/// <summary>
/// Overload of the plus operator
/// </summary>
tk::Vector3 tk::Vector3::operator+(const Vector3 t_right) const
{
	return Vector3(x + t_right.x, y + t_right.y, z + t_right.z);
}

/// <summary>
/// Overload of the minus operator
/// </summary>
tk::Vector3 tk::Vector3::operator-(const Vector3 t_right) const
{
	return Vector3(x - t_right.x, y - t_right.y, z - t_right.z);
}

/// <summary>
/// Overload of the multiplication operator
/// </summary>
tk::Vector3 tk::Vector3::operator*(const double t_scalar) const
{
	double a = x * t_scalar;
	double b = y * t_scalar;
	double c = z * t_scalar;
	return Vector3(a, b, c);
}

/// <summary>
/// Overload of the division operator
/// </summary>
tk::Vector3 tk::Vector3::operator/(const double t_divisor) const
{
	double a;
	double b;
	double c;

	if (t_divisor == 0)
	{
		a = 0;
		b = 0;
		c = 0;
	}
	else
	{
		a = x / t_divisor;
		b = y / t_divisor;
		c = z / t_divisor;
	}

	return Vector3(a, b, c);
}

/// <summary>
/// Overload of the plusequals operator
/// </summary>
tk::Vector3 tk::Vector3::operator+=(const Vector3 t_right)
{
	x += t_right.x;
	y += t_right.y;
	z += t_right.z;
	return Vector3(x, y, z);
}

/// <summary>
/// Overload of the minusequals operator
/// </summary>
tk::Vector3 tk::Vector3::operator-=(const Vector3 t_right)
{
	x -= t_right.x;
	y -= t_right.y;
	z -= t_right.z;
	return Vector3(x, y, z);
}

/// <summary>
/// Overload of the unary negative operator
/// </summary>
tk::Vector3 tk::Vector3::operator-()
{
	return Vector3(x * -1, y * -1, z * -1);
}

/// <summary>
/// Overload of the equality operator
/// </summary>
bool tk::Vector3::operator==(const Vector3 t_right) const
{
	const bool result{ x == t_right.x && y == t_right.y && t_right.z };
	return result;
}

/// <summary>
/// Overload of the inequality operator
/// </summary>
bool tk::Vector3::operator!=(const Vector3 t_right) const
{
	const bool result{ x != t_right.x || y != t_right.y || z != t_right.z };
	return result;
}

#pragma endregion