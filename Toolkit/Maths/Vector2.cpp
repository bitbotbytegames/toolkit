#include "Vector2.h"

#define PI 3.14159265358979323846

#pragma region constructors

/// <summary>
/// Default constructor for the Vector2 class
/// </summary>
tk::Vector2::Vector2() : x{ 0.0 }, y{ 0.0 }
{

}

/// <summary>
/// Constructor taking doubles
/// </summary>
tk::Vector2::Vector2(double t_x, double t_y) : x{ static_cast<double>(t_x) }, y{ static_cast<double>(t_y) }
{

}

/// <summary>
/// Constructor taking an SFML Vector2i
/// </summary>
tk::Vector2::Vector2(sf::Vector2i t_vector) : x{ static_cast<double>(t_vector.x) }, y{ static_cast<double>(t_vector.y) }
{

}

/// <summary>
/// Constructor taking an SFML Vector2f
/// </summary>
tk::Vector2::Vector2(sf::Vector2f t_vector) : x{ static_cast<double>(t_vector.x) }, y{ static_cast<double>(t_vector.y) }
{

}

/// <summary>
/// Constructor taking an SFML Vector2u
/// </summary>
tk::Vector2::Vector2(sf::Vector2u t_vector) : x{ static_cast<double>(t_vector.x) }, y{ static_cast<double>(t_vector.y) }
{

}

#pragma endregion

/// <summary>
/// Destructor for the Vector2 class
/// </summary>
tk::Vector2::~Vector2()
{

}

#pragma region methods

/// <summary>
/// Length
/// </summary>
double tk::Vector2::length() const
{
	const double result = std::sqrt(x * x + y * y);
	return result;
}

/// <summary>
/// Length squared
/// </summary>
double tk::Vector2::lengthSquared() const
{
	const double result = x * x + y * y;
	return result;
}

/// <summary>
/// Unit - Returns a vector with a length of 1
/// </summary>
tk::Vector2 tk::Vector2::unit() const
{	
	double a = x / (std::sqrt(x * x + y * y));
	double b = y / (std::sqrt(x * x + y * y));
	return Vector2(a, b);
}

/// <summary>
/// Normalise - Changes the length of a supplied vector to 1
/// </summary>
void tk::Vector2::normalise()
{
	x = x / (std::sqrt(x * x + y * y));
	y = y / (std::sqrt(x * x + y * y));
}

/// <summary>
/// Rotate vector (in radians)
/// </summary>
tk::Vector2 tk::Vector2::rotateBy(float t_angleRadians)
{
	double a = cos(t_angleRadians) * x - sin(t_angleRadians) * y;
	double b = sin(t_angleRadians) * x + cos(t_angleRadians) * y;
	return Vector2(a, b);
}

/// <summary>
/// Return the angle between two vectors (in degrees)
/// </summary>
double tk::Vector2::angleBetween(Vector2 t_vector) const
{	
	double dotAB = (x * t_vector.x) + (y * t_vector.y);
	double magA = std::sqrt((x * x) + (y * y));
	double magB = std::sqrt((t_vector.x * t_vector.x) + (t_vector.y * t_vector.y));
	double magTotal = magA * magB;
	double angle = dotAB / magTotal; 
	const double result = acos(angle) * 180.0 / PI;	
	return result;
}

/// <summary>
/// Dot product
/// </summary>
double tk::Vector2::dot(Vector2 t_vector) const
{
	const double dotProduct = (x * t_vector.x) + (y * t_vector.y);
	return dotProduct;
}

/// <summary>
/// Projection - The answer will be parallel to the supplied vector
/// </summary>
tk::Vector2 tk::Vector2::projection(const Vector2 t_vector) const
{
	double dotAB = (x * t_vector.x) + (y * t_vector.y);
	double magB = std::sqrt((x * x) + (y * y));
	double division = dotAB / (magB * magB);
	double a = division * x;
	double b = division * y;
	return Vector2(a, b);
}

/// <summary>
/// Rejection - The answer will be orthogonal to the supplied vector
/// </summary>
tk::Vector2 tk::Vector2::rejection(const Vector2 t_vector) const
{
	double dotAB = (x * t_vector.x) + (y * t_vector.y);
	double magB = std::sqrt((x * x) + (y * y));
	double division = dotAB / (magB * magB);
	double a = t_vector.x - (division * x);
	double b = t_vector.y - (division * y);
	return Vector2(a, b);
}

/// <summary>
/// Truncate
/// </summary>
tk::Vector2 tk::Vector2::truncate(Vector2 t_vector, float const t_max)
{
	if (t_vector.length() > t_max)
	{
		t_vector = t_vector.unit();
		t_vector = t_vector * t_max;
	}

	return t_vector;
}

/// <summary>
/// Create a string for the vector in the form [x,y]
/// using std::to_string
/// </summary>
const std::string tk::Vector2::toString()
{
	const std::string output = "[" + std::to_string(x) + "," + std::to_string(y) + "]";
	return output;
}

#pragma endregion

#pragma region operators

/// <summary>
/// Overload of the plus operator
/// </summary>
tk::Vector2 tk::Vector2::operator+(const Vector2 t_vector) const
{
	return Vector2(x + t_vector.x, y + t_vector.y);
}

/// <summary>
/// Overload of the minus operator
/// </summary>
tk::Vector2 tk::Vector2::operator-(const Vector2 t_vector) const
{
	return Vector2(x - t_vector.x, y - t_vector.y);
}

/// <summary>
/// Overload of the multiplication operator (double)
/// </summary>
tk::Vector2 tk::Vector2::operator*(const double t_scalar) const
{
	double a = x * t_scalar;
	double b = y * t_scalar;
	return Vector2(a, b);
}

/// <summary>
/// Overload of the multiplication operator (float)
/// </summary>
tk::Vector2 tk::Vector2::operator*(const float t_scalar) const
{
	double a = x * t_scalar;
	double b = y * t_scalar;
	return Vector2(a, b);
}

/// <summary>
/// Overload of the multiplication operator (int)
/// </summary>
tk::Vector2 tk::Vector2::operator*(const int t_scalar) const
{
	double a = x * t_scalar;
	double b = y * t_scalar;
	return Vector2(a, b);
}

/// <summary>
/// Overload of the division operator (double)
/// </summary>
tk::Vector2 tk::Vector2::operator/(const double t_divisor) const
{
	double a = x / t_divisor;
	double b = y / t_divisor;
	return Vector2(a, b);
}

/// <summary>
/// Overload of the division operator (float)
/// </summary>
tk::Vector2 tk::Vector2::operator/(const float t_divisor) const
{
	double a = x / t_divisor;
	double b = y / t_divisor;
	return Vector2(a, b);
}

/// <summary>
/// Overload of the division operator (int)
/// </summary>
tk::Vector2 tk::Vector2::operator/(const int t_divisor) const
{
	double a = x / t_divisor;
	double b = y / t_divisor;
	return Vector2(a, b);
}

/// <summary>
/// Overload of the plusequals operator
/// </summary>
tk::Vector2 tk::Vector2::operator+=(const Vector2 t_vector)
{
	x += t_vector.x;
	y += t_vector.y;
	return Vector2(x, y);
}

/// <summary>
/// Overload of the minusequals operator
/// </summary>
tk::Vector2 tk::Vector2::operator-=(const Vector2 t_vector)
{
	x -= t_vector.x;
	y -= t_vector.y;
	return Vector2(x, y);
}

/// <summary>
/// Overload of the unary negative operator
/// </summary>
tk::Vector2 tk::Vector2::operator-()
{
	return Vector2(x * -1, y * -1);
}

/// <summary>
/// Overload of the equality operator
/// </summary>
bool tk::Vector2::operator==(const Vector2 t_vector) const
{	
	const bool result{ x == t_vector.x && y == t_vector.y };
	return result;
}

/// <summary>
/// Overload of the inequality operator
/// </summary>
bool tk::Vector2::operator!=(const Vector2 t_vector) const
{
	const bool result{ x != t_vector.x || y != t_vector.y };
	return result;
}

#pragma endregion
