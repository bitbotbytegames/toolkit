#ifndef MATRIX3_H
#define MATRIX3_H

#include "Vector3.h"

#include <string>

namespace tk
{
	class Matrix3
	{
	public:
		Matrix3();		
		Matrix3(double t_a11, double t_a12, double t_a13, double t_a21, double t_a22, double t_a23, double t_a31, double t_a32, double t_a33);
		Matrix3(tk::Vector3 t_row1, tk::Vector3 t_row2, tk::Vector3 t_row3);
		~Matrix3();

		Matrix3 transpose() const;
		double determinant() const;
		Matrix3 inverse() const;
		tk::Vector3 getRow(const int t_row) const;
		tk::Vector3 getColumn(const int t_column) const;
		static Matrix3 rotationX(const double t_angleRadians);
		static Matrix3 rotationY(const double t_angleRadians);
		static Matrix3 rotationZ(const double t_angleRadians);
		static Matrix3 translation(const tk::Vector3 t_displacement);
		static Matrix3 scale(const double t_scalingFactor);
		std::string toString() const;

		bool operator==(const Matrix3 t_matrix) const;
		bool operator!=(const Matrix3 t_matrix) const;
		Matrix3 operator+(const Matrix3 t_matrix) const;
		Matrix3 operator-(const Matrix3 t_matrix) const;
		Matrix3 operator*(const Matrix3 t_matrix) const;
		tk::Vector3 operator*(const tk::Vector3 t_vector) const;
		Matrix3 operator*(const double t_scalar) const;

	private:
		double m_11;
		double m_12;
		double m_13;
		double m_21;
		double m_22;
		double m_23;
		double m_31;
		double m_32;
		double m_33;
	};
}

#endif //!MATRIX3_H