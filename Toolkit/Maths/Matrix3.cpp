﻿#include "Matrix3.h"

#define PI 3.14159265358979323846

#pragma region constructors

/// <summary>
/// Default constructor for the Matrix3 class
/// </summary>
tk::Matrix3::Matrix3()
{
	m_11 = 0.0;
	m_21 = 0.0;
	m_31 = 0.0;

	m_12 = 0.0;
	m_22 = 0.0;	
	m_32 = 0.0;	

	m_13 = 0.0;
	m_23 = 0.0;
	m_33 = 0.0;
}

/// <summary>
/// Constructor taking doubles
/// </summary>
tk::Matrix3::Matrix3(double t_a11, double t_a12, double t_a13, double t_a21, double t_a22, double t_a23, double t_a31, double t_a32, double t_a33)
{
	m_11 = static_cast<double>(t_a11);
	m_21 = static_cast<double>(t_a21);
	m_31 = static_cast<double>(t_a31);

	m_12 = static_cast<double>(t_a12);
	m_22 = static_cast<double>(t_a22);
	m_32 = static_cast<double>(t_a32);	

	m_13 = static_cast<double>(t_a13);
	m_23 = static_cast<double>(t_a23);
	m_33 = static_cast<double>(t_a33);
}

/// <summary>
/// Constructor taking 3 x Vector3
/// </summary>
tk::Matrix3::Matrix3(Vector3 t_row1, Vector3 t_row2, Vector3 t_row3)
{
	m_11 = t_row1.x;
	m_21 = t_row2.x;
	m_31 = t_row3.x;
	
	m_12 = t_row1.y;
	m_22 = t_row2.y;
	m_32 = t_row3.y;

	m_13 = t_row1.z;
	m_23 = t_row2.z;
	m_33 = t_row3.z;
}

#pragma endregion

/// <summary>
/// Destructor for the Matrix3 class
/// </summary>
tk::Matrix3::~Matrix3()
{

}

#pragma region methods

/// <summary>
/// Transpose
/// </summary>
tk::Matrix3 tk::Matrix3::transpose() const
{
	double a_11, a_12, a_13, a_21, a_22, a_23, a_31, a_32, a_33;

	a_11 = m_11;
	a_12 = m_21;
	a_13 = m_31;
	a_21 = m_12;
	a_22 = m_22;
	a_23 = m_32;
	a_31 = m_13;
	a_32 = m_23;
	a_33 = m_33;

	return Matrix3(a_11, a_12, a_13, a_21, a_22, a_23, a_31, a_32, a_33);
}

/// <summary>
/// Determinant
/// </summary>
double tk::Matrix3::determinant() const
{
	double result = m_11 * (m_22 * m_33 - m_23 * m_32) -
		m_12 * (m_21 * m_33 - m_31 * m_23) +
		m_13 * (m_21 * m_32 - m_22 * m_31);
	return result;
}

/// <summary>
/// Inverse
/// </summary>
tk::Matrix3 tk::Matrix3::inverse() const
{
	double a_11, a_12, a_13, a_21, a_22, a_23, a_31, a_32, a_33;

	double det = determinant();
	Matrix3 result;

	if (det != 0)
	{
		Matrix3 trans = transpose();

		// Get matrix of cofactors from transpose of original matrix
		// Then get adjugate matrix by changing signs
		// Divide each term by the determinant
		a_11 = (trans.m_22 * trans.m_33 - trans.m_23 * trans.m_32) * 1 / det;
		a_12 = ((trans.m_21 * trans.m_33 - trans.m_23 * trans.m_31) * -1) * 1 / det;
		a_13 = (trans.m_21 * trans.m_32 - trans.m_22 * trans.m_31) * 1 / det;
		a_21 = ((trans.m_12 * trans.m_33 - trans.m_13 * trans.m_32) * -1) * 1 / det;
		a_22 = (trans.m_11 * trans.m_33 - trans.m_13 * trans.m_31) * 1 / det;
		a_23 = ((trans.m_11 * trans.m_32 - trans.m_12 * trans.m_31) * -1) * 1 / det;
		a_31 = (trans.m_12 * trans.m_23 - trans.m_13 * trans.m_22) * 1 / det;
		a_32 = ((trans.m_11 * trans.m_23 - trans.m_13 * trans.m_21) * -1) * 1 / det;
		a_33 = (trans.m_11 * trans.m_22 - trans.m_12 * trans.m_21) * 1 / det;

		result = Matrix3(a_11, a_12, a_13, a_21, a_22, a_23, a_31, a_32, a_33);
	}
	else
	{
		// If there's no inverse then return the original matrix
		result = Matrix3(m_11, m_12, m_13, m_21, m_22, m_23, m_31, m_32, m_33);
	}

	return result;
}

/// <summary>
/// Row
/// </summary>
tk::Vector3 tk::Matrix3::getRow(const int t_row) const
{
	Vector3 rowResult(0.0, 0.0, 0.0);

	if (t_row >= 0 && t_row < 3)
	{
		switch (t_row)
		{
		case 0:
			rowResult.x = m_11;
			rowResult.y = m_12;
			rowResult.z = m_13;
			break;

		case 1:
			rowResult.x = m_21;
			rowResult.y = m_22;
			rowResult.z = m_23;
			break;

		case 2:
			rowResult.x = m_31;
			rowResult.y = m_32;
			rowResult.z = m_33;
			break;
		}
	}

	return rowResult;
}

/// <summary>
/// Column
/// </summary>
tk::Vector3 tk::Matrix3::getColumn(const int t_column) const
{
	Vector3 columnResult(0.0, 0.0, 0.0);

	if (t_column >= 0 && t_column < 3)
	{
		switch (t_column)
		{
		case 0:
			columnResult.x = m_11;
			columnResult.y = m_21;
			columnResult.z = m_31;
			break;

		case 1:
			columnResult.x = m_12;
			columnResult.y = m_22;
			columnResult.z = m_32;
			break;

		case 2:
			columnResult.x = m_13;
			columnResult.y = m_23;
			columnResult.z = m_33;
			break;
		}
	}

	return columnResult;
}

/// <summary>
/// X axis rotation
/// </summary>
tk::Matrix3 tk::Matrix3::rotationX(const double t_angleRadians)
{
	double a_11, a_12, a_13, a_21, a_22, a_23, a_31, a_32, a_33;

	a_11 = 1;
	a_12 = 0;
	a_13 = 0;
	a_21 = 0;
	a_22 = std::cos(t_angleRadians);
	a_23 = std::sin(t_angleRadians * -1.0);
	a_31 = 0;
	a_32 = std::sin(t_angleRadians);
	a_33 = std::cos(t_angleRadians);

	return Matrix3(a_11, a_12, a_13, a_21, a_22, a_23, a_31, a_32, a_33);
}

/// <summary>
/// Y axis rotation
/// </summary>
/// <returns>Rotation matrix of given angle on Y axis</returns>
tk::Matrix3 tk::Matrix3::rotationY(const double t_angleRadians)

{
	double a_11, a_12, a_13, a_21, a_22, a_23, a_31, a_32, a_33;

	a_11 = std::cos(t_angleRadians);
	a_12 = 0;
	a_13 = std::sin(t_angleRadians);
	a_21 = 0;
	a_22 = 1;
	a_23 = 0;
	a_31 = std::sin(t_angleRadians * -1.0);
	a_32 = 0;
	a_33 = std::cos(t_angleRadians);

	return Matrix3(a_11, a_12, a_13, a_21, a_22, a_23, a_31, a_32, a_33);
}

/// <summary>
/// Z axis rotation
/// </summary>
tk::Matrix3 tk::Matrix3::rotationZ(const double t_angleRadians)
{
	double a_11, a_12, a_13, a_21, a_22, a_23, a_31, a_32, a_33;

	a_11 = std::cos(t_angleRadians);
	a_12 = std::sin(t_angleRadians * -1.0);
	a_13 = 0;
	a_21 = std::sin(t_angleRadians);
	a_22 = std::cos(t_angleRadians);
	a_23 = 0;
	a_31 = 0;
	a_32 = 0;
	a_33 = 1;

	return Matrix3(a_11, a_12, a_13, a_21, a_22, a_23, a_31, a_32, a_33);
}

/// <summary>
/// Translation
/// </summary>
tk::Matrix3 tk::Matrix3::translation(const Vector3 t_displacement)
{
	double a_11, a_12, a_13, a_21, a_22, a_23, a_31, a_32, a_33;

	a_11 = 1;
	a_12 = 0;
	a_13 = t_displacement.x;
	a_21 = 0;
	a_22 = 1;
	a_23 = t_displacement.y;
	a_31 = 0;
	a_32 = 0;
	a_33 = 1;

	return Matrix3(a_11, a_12, a_13, a_21, a_22, a_23, a_31, a_32, a_33);
}

/// <summary>
/// Scale
/// </summary>
tk::Matrix3 tk::Matrix3::scale(const double t_scalingFactor)
{
	double a_11, a_12, a_13, a_21, a_22, a_23, a_31, a_32, a_33;

	a_11 = t_scalingFactor;
	a_12 = 0;
	a_13 = 0;
	a_21 = 0;
	a_22 = t_scalingFactor;
	a_23 = 0;
	a_31 = 0;
	a_32 = 0;
	a_33 = 1;

	return Matrix3(a_11, a_12, a_13, a_21, a_22, a_23, a_31, a_32, a_33);
}

/// <summary>
/// Create a string for the matrix
/// using std::to_string
/// </summary>
std::string tk::Matrix3::toString() const
{
	const std::string output = "[" + std::to_string(m_11) + "," + std::to_string(m_12) + "," + std::to_string(m_13) +
							"]\n[" + std::to_string(m_21) + "," + std::to_string(m_22) + "," + std::to_string(m_23) +
							"]\n[" + std::to_string(m_31) + "," + std::to_string(m_32) + "," + std::to_string(m_33) + "]";
	return output;
}

#pragma endregion

#pragma region operators

/// <summary>
/// Overload of the equality operator
/// </summary>
bool tk::Matrix3::operator==(const Matrix3 t_matrix) const
{
	const bool result{ m_11 == t_matrix.m_11 && m_12 == t_matrix.m_12 && m_13 == t_matrix.m_13 && 
		m_21 == t_matrix.m_21 && m_22 == t_matrix.m_22 && m_23 == t_matrix.m_23 &&
		m_31 == t_matrix.m_31 && m_32 == t_matrix.m_32 && m_33 == t_matrix.m_33 };
	return result;
}

/// <summary>
/// Overload of the inequality operator
/// </summary>
bool tk::Matrix3::operator!=(const Matrix3 t_matrix) const
{
	const bool result{ m_11 != t_matrix.m_11 || m_12 != t_matrix.m_12 || m_13 != t_matrix.m_13 ||
		m_21 != t_matrix.m_21 || m_22 != t_matrix.m_22 || m_23 != t_matrix.m_23 ||
		m_31 != t_matrix.m_31 || m_32 != t_matrix.m_32 || m_33 != t_matrix.m_33 };
	return result;
}

/// <summary>
/// Overload of the plus operator
/// </summary>
tk::Matrix3 tk::Matrix3::operator+(const Matrix3 t_matrix) const
{
	return Matrix3(m_11 + t_matrix.m_11, m_12 + t_matrix.m_12, m_13 + t_matrix.m_13,
		m_21 + t_matrix.m_21, m_22 + t_matrix.m_22, m_23 + t_matrix.m_23,
		m_31 + t_matrix.m_31, m_32 + t_matrix.m_32, m_33 + t_matrix.m_33);
}

/// <summary>
/// Overload of the minus operator
/// </summary>
tk::Matrix3 tk::Matrix3::operator-(const Matrix3 t_matrix) const
{
	return Matrix3(m_11 - t_matrix.m_11, m_12 - t_matrix.m_12, m_13 - t_matrix.m_13,
		m_21 - t_matrix.m_21, m_22 - t_matrix.m_22, m_23 - t_matrix.m_23,
		m_31 - t_matrix.m_31, m_32 - t_matrix.m_32, m_33 - t_matrix.m_33);
}

/// <summary>
/// Overload of the multiplication operator (matrix x matrix)
/// </summary>
tk::Matrix3 tk::Matrix3::operator*(const Matrix3 t_matrix) const
{
	double a_11, a_12, a_13, a_21, a_22, a_23, a_31, a_32, a_33;	

	a_11 = m_11 * t_matrix.m_11 + m_12 * t_matrix.m_21 + m_13 * t_matrix.m_31;
	a_12 = m_11 * t_matrix.m_12 + m_12 * t_matrix.m_22 + m_13 * t_matrix.m_32;
	a_13 = m_11 * t_matrix.m_13 + m_12 * t_matrix.m_23 + m_13 * t_matrix.m_33;
	a_21 = m_21 * t_matrix.m_11 + m_22 * t_matrix.m_21 + m_23 * t_matrix.m_31;
	a_22 = m_21 * t_matrix.m_12 + m_22 * t_matrix.m_22 + m_23 * t_matrix.m_32;
	a_23 = m_21 * t_matrix.m_13 + m_22 * t_matrix.m_23 + m_23 * t_matrix.m_33;
	a_31 = m_31 * t_matrix.m_11 + m_32 * t_matrix.m_21 + m_33 * t_matrix.m_31;
	a_32 = m_31 * t_matrix.m_12 + m_32 * t_matrix.m_22 + m_33 * t_matrix.m_32;
	a_33 = m_31 * t_matrix.m_13 + m_32 * t_matrix.m_23 + m_33 * t_matrix.m_33;

	return Matrix3(a_11, a_12, a_13, a_21, a_22, a_23, a_31, a_32, a_33);
}

/// <summary>
/// Overload of the multiplication operator (matrix x vector)
/// </summary>
tk::Vector3 tk::Matrix3::operator*(const Vector3 t_vector) const
{
	double a, b, c;

	a = m_11 * t_vector.x + m_12 * t_vector.y + m_13 * t_vector.z;
	b = m_21 * t_vector.x + m_22 * t_vector.y + m_23 * t_vector.z;
	c = m_31 * t_vector.x + m_32 * t_vector.y + m_33 * t_vector.z;

	return Vector3(a, b, c);
}

/// <summary>
/// Overload of the multiplication operator (matrix x scalar)
/// </summary>
tk::Matrix3 tk::Matrix3::operator*(const double t_scalar) const
{
	double a_11, a_12, a_13, a_21, a_22, a_23, a_31, a_32, a_33;

	a_11 = m_11 * t_scalar;
	a_12 = m_12 * t_scalar;
	a_13 = m_13 * t_scalar;
	a_21 = m_21 * t_scalar;
	a_22 = m_22 * t_scalar;
	a_23 = m_23 * t_scalar;
	a_31 = m_31 * t_scalar;
	a_32 = m_32 * t_scalar;
	a_33 = m_33 * t_scalar;

	return Matrix3(a_11, a_12, a_13, a_21, a_22, a_23, a_31, a_32, a_33);
}

#pragma endregion
